import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import ArticleCard from '../src/components/ArticleCard';

describe('ArticleCard', () => {
    it('affiche le titre et le contenu de l\'article', () => {
      // Créer un faux article à passer au composant
      const fakeArticle = {
        title: 'Titre Test',
        content: 'Contenu Test',
      };
  
      // Render le composant ArticleCard avec le faux article
      render(<ArticleCard article={fakeArticle} />);
  
      // Vérifier que le titre et le contenu sont présents dans le document
      expect(screen.getByText('Titre Test')).toBeInTheDocument();
      expect(screen.getByText('Contenu Test')).toBeInTheDocument();
      expect(screen.getByText('Read more (It doesn\'t work actually)')).toBeInTheDocument();
    });
  });