import { render, screen } from '@testing-library/react';
import CustomAppbar from '../src/components/CustomAppbar'; // Ajustez le chemin d'importation selon votre structure de dossiers

describe('CustomAppbar', () => {
  it('affiche le titre dans l\'AppBar', () => {
    // Render le composant CustomAppbar
    render(<CustomAppbar />);

    // Vérifier que le titre est présent dans le document
    expect(screen.getByText("L'appli avec des articles ouais")).toBeInTheDocument();
  });
});