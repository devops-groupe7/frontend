import { render, screen, fireEvent } from '@testing-library/react';
import SnackbarHandler from '../src/components/SnackbarHandler'; // Ajustez le chemin d'importation selon votre structure de dossiers

describe('SnackbarHandler', () => {
  it('affiche un message de succès', () => {
    const handleClose = jest.fn();
    render(<SnackbarHandler isOpen={true} type="success" message="Action réussie !" onClose={handleClose} />);
    
    // Vérifie que le Snackbar affiche le message correct
    expect(screen.getByText("Action réussie !")).toBeInTheDocument();
    expect(screen.getByRole('alert')).toHaveClass('MuiAlert-filledSuccess');
  });

  it('appelle onClose lorsqu\'il est fermé', () => {
    const handleClose = jest.fn();
    render(<SnackbarHandler isOpen={true} type="success" message="Action réussie !" onClose={handleClose} />);
    
    // Simule la fermeture du Snackbar
    const closeButton = screen.getByLabelText(/close/i);
    fireEvent.click(closeButton);

    // Vérifie que la fonction handleClose a été appelée
    expect(handleClose).toHaveBeenCalledTimes(1);
  });
});
